# Inputmask Widget Formatter

## Table of contents

  - Introduction
  - Requirements
  - Installation
  - Configuration
  - Maintainers


## Introduction

Allow the text field widget inputmask formatter.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
https://www.drupal.org/docs/extending-drupal/installing-drupal-modules.

## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.

## Maintainers

Current maintainers:
 - Deepak Bhati (heni_deepak) - https://www.drupal.org/u/heni_deepak
 - Radheshyam Kumawat (radheymkumar) - https://www.drupal.org/u/radheymkumar
